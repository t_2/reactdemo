import React from 'react';
import ReactDOM from 'react-dom';
import {Button, Checkbox, InputText} from 'primereact';
import 'primereact/resources/primeng.min.css';
import 'primereact/resources/themes/omega/theme.css';
import 'font-awesome/css/font-awesome.css';
import './index.css';
const {render} = ReactDOM

const marginStyle = {
    marginBottom: 20,
    marginTop: 20
}


const activeButton = {
    class: "ui-c"
};

const stylePassiveButton = {
    marginBottom: 20
}

function onChange(e) {
    if (e.checked) {
        e.parent.jQuery.checked(true);
        this.checked = true;
    } else {
        this.checked = false;

    }
    alert("test");
}


class CustomCheckbox extends Checkbox {
    render() {
        return (
            <div class="chkbox-container" style={this.props.style}>
                <div id="form:internal-user2" className="ui-chkbox ui-widget">
                    <div className="ui-helper-hidden-accessible"><input id="form:internal-user_input"
                                                                        name="form:internal-user_input" type="checkbox"
                                                                        autocomplete="off" aria-checked="false"/></div>
                    <div className="ui-chkbox-box ui-widget ui-corner-all ui-state-default"><span
                        className="ui-chkbox-icon ui-icon ui-icon-blank ui-c"></span></div>
                </div>
                <label style={{marginLeft: 10}} className="ui-outputlabel ui-widget" for="form:internal-user_input2">служебный вход</label>
            </div>
        );
    }
}


render(
    <div>
        <InputText placeholder="Имя пользователя" id="login" style={marginStyle} />
        <InputText placeholder="Пароль" id="password" />
        <CustomCheckbox label="Служебный вход" id="internal_user" style={marginStyle} onChange={onChange}/>
        <Button label="Войти" style={Object.assign(marginStyle, activeButton)}  icon="fa fa-user"/>
        <Button label="Вспомнить пароль"
            icon="fa fa-key"
            style={stylePassiveButton}
            onClick={() => {window.open('/rigel/forgot-password.xhtml', '_self')}}
            className="ui-button-info" />
    </div>
    ,
    document.getElementById('root')
);