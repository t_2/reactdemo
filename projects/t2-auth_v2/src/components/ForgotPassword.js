import React from 'react';
import {Link} from 'react-router';
import '../styles/about-page.css';

// Since this component is simple and static, there's no parent container for it.
const ForgotPassword = () => {
  return (
    <div>
      <form id="form" name="form" method="post" action="/rigel/forgot-password.xhtml">
        <input type="hidden" name="form" value="form"/>
        <span id="form:growl"/>
        <div id="form:panel" className="ui-panel ui-widget ui-widget-content ui-corner-all"
             data-widget="widget_form_panel">
          <div id="form:panel_header" style={{color: 'white', backgroundColor: '#2d353c'}}
               className="ui-panel-titlebar ui-helper-clearfix ui-corner-all">
            <span className="ui-panel-title">Восстановление пароля</span></div>
          <div id="form:panel_content" className="ui-panel-content ui-widget-content">
            <table>
              <tbody>
              <tr>
                <td><label id="form:j_idt7" className="ui-outputlabel ui-widget">Логин: <span
                  className="ui-outputlabel-rfi">*</span></label></td>
                <td><input id="form:login" name="form:login" type="text"
                           style={{width: '270px'}} aria-required="true"
                           className="ui-inputfield ui-inputtext ui-widget ui-state-default ui-corner-all"/></td>
                <td/>
              </tr>

              <tr>
                <td><label id="form:j_idt9" className="ui-outputlabel ui-widget">
                  Код подтверждения: </label></td>
                <td><input id="form:code" name="form:code" type="text"
                           style={{width: '270px'}}
                           className="ui-inputfield ui-inputtext ui-widget ui-state-default ui-corner-all"/></td>
              </tr>
              <tr>
                <td>
                  <button id="form:send-code-button" name="form:send-code-button"
                          className="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only"
                          type="submit"><Link to="/sfasfa"><span className="ui-button-text ui-c"
                                                                 style={{color: 'white'}}>Выслать код подтверждения</span></Link>
                  </button>
                </td>
              </tr>
              </tbody>
            </table>
          </div>
        </div>
      </form>
    </div>
  );
};

export default ForgotPassword;
