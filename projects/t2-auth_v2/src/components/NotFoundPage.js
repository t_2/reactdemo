import React from 'react';
import {Link} from 'react-router';

class NotFoundPage extends React.Component {
  componentWillMount() {
    document.body.classList.add('exception-body');
    document.body.classList.add('notfound-page');
    document.body.classList.remove('login-body');
  }

  componentWillUnmount() {
    document.body.classList.remove('exception-body');
    document.body.classList.remove('notfound-page');
    document.body.classList.add('login-body');
  }

  render() {
    return (
      <div>
        <div className="exception-top" style={{height: '300px'}}><img
          src="https://t2.maykor.com/rigel/javax.faces.resource/images/icon-404.png.xhtml?ln=poseidon-layout"
          alt=""/>
        </div>
        <div className="exception-bottom">
          <div className="exception-wrapper">
            <span className="exception-summary">Доступ невозможен</span>
            <span className="exception-detail">Запрашиваемый ресурс не найден</span>
            <button id="j_idt9" name="j_idt9" type="button"
                    className="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-icon-left turquoise-btn"
            ><span className="ui-button-icon-left ui-icon ui-c fa fa-home"/><span
              className="ui-button-text ui-c"><Link style={{color: 'white'}} to="/">Главная страница</Link></span>
            </button>
            <img id="j_idt10"
                 src="https://t2.maykor.com/rigel/javax.faces.resource/images/logo-icon-white.png.xhtml?ln=poseidon-layout"
                 alt=""
                 className="logo-icon"/>
            <span className="exception-footer">T2 - Версия: master.170</span>
          </div>
        </div>
      </div>
    );
  }
}

export default NotFoundPage;
