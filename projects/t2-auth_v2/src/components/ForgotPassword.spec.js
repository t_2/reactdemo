import React from 'react';
import {shallow} from 'enzyme';
import ForgotPassword from './ForgotPassword';

describe('<ForgotPassword />', () => {
  it('should have Восстановление пароля', () => {
    const wrapper = shallow(<ForgotPassword />);
    const actual = wrapper.find('.ui-panel-title').text();
    const expected = 'Восстановление пароля';

    expect(actual).toEqual(expected);
  });

  it('should have an action with \'/rigel/forgot-password.xhtml\' href', () => {
    const wrapper = shallow(<ForgotPassword />);
    const actual = wrapper.find('form').prop('action');
    const expected = '/rigel/forgot-password.xhtml';

    expect(actual).toEqual(expected);
  });

  it('should link to an unknown route path', () => {
    const wrapper = shallow(<ForgotPassword />);
    const actual = wrapper.findWhere(n => n.prop('to') === '/sfasfa').length;
    const expected = 1;

    expect(actual).toEqual(expected);
  });
});
