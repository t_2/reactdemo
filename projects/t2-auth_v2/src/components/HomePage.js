import React from 'react';
import {Link} from 'react-router';
import {Button, Checkbox, InputText} from 'primereact';
import 'primereact/resources/primeng.min.css';
import 'primereact/resources/themes/omega/theme.css';

const marginStyle = {
  marginBottom: 20,
  marginTop: 20
};
const activeButton = {
  class: "ui-c"
};
const stylePassiveButton = {
  marginBottom: 20
};



class CustomCheckbox extends Checkbox {
  render() {
    return (
      <div className="chkbox-container" style={this.props.style}>
        <div id="form:internal-user2" className="ui-chkbox ui-widget">
          <div className="ui-helper-hidden-accessible"><input id="form:internal-user_input"
                                                              name="form:internal-user_input" type="checkbox"
                                                              aria-checked="false"/></div>
          <div className="ui-chkbox-box ui-widget ui-corner-all ui-state-default"><span
            className="ui-chkbox-icon ui-icon ui-icon-blank ui-c"/></div>
        </div>
        <label style={{marginLeft: 10}} className="ui-outputlabel ui-widget">служебный
          вход</label>
      </div>
    );
  }
}

const HomePage = () => {
  return (
    <div>
      <span id="growl"/>
      <form id="form" name="form" method="post" action="/rigel/login.xhtml">
        <input type="hidden" name="form" value="form"/>
        <div className="login-panel ui-fluid">
          <div className="ui-g">
            <div className="ui-g-12 logo-container">
              <img id="form:j_idt7"
                   src="https://t2.maykor.com/rigel/javax.faces.resource/images/login/login-logo.png.xhtml?ln=poseidon-layout"
              />
            </div>
            <InputText placeholder="Имя пользователя" id="login" style={marginStyle}/>
            <InputText placeholder="Пароль" id="password"/>
            <CustomCheckbox label="Служебный вход" id="internal_user" style={{...marginStyle, ...{color: 'black'}}}/>
            <Link style={{color: 'white', width: '100%'}} to="/sfasfa">
              <Button label="Войти"
                      style={{...marginStyle, ...activeButton}}
                      icon="fa fa-user"/></Link>

            <Link style={{color: 'white', width: '100%'}} to="/forgot-password">
              <Button label="Вспомнить пароль"
                      icon="fa fa-key"
                      style={stylePassiveButton}
                      className="ui-button-info"/>
            </Link>
          </div>
        </div>
      </form>
    </div>
  );
};
export default HomePage;
