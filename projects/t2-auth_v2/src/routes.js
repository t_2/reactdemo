import React from 'react';
import { Route, IndexRoute } from 'react-router';

import App from './components/App';
import HomePage from './components/HomePage';
import ForgotPassword from './components/ForgotPassword';
import NotFoundPage from './components/NotFoundPage';


export default (
  <Route  path="/" component={App}>
    <IndexRoute component={HomePage}/>
    <Route path="forgot-password" component={ForgotPassword}/>
    <Route path="*" component={NotFoundPage}/>
  </Route>
);
