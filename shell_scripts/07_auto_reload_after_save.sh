#!/bin/bash
echo "1. Установить сервис для автоперезагрузки страницы после сохранения файла React (JavaScript/HTML)? [Enter]"; read dummy;
echo https://www.npmjs.com/package/livereload
npm install -g livereload
echo "2. Добавить в body <script>document.write('<script src="http://' + (location.host || 'localhost').split(':')[0] +':35729/livereload.js?snipver=1"></' + 'script>')</script>"
echo "3. Включить перезагрузку страницы после сохранения файла в папках folder1 и folder2 c паузой в 1 секунду"; read dummy;
livereload "/folder1/,/folder2/" --wait 2000
#Альтернатива http://livereload.com/
#+плагин для Chrome https://chrome.google.com/webstore/detail/livereload/jnihajbhpnppcggbcgedagnkighmdlei?hl=en