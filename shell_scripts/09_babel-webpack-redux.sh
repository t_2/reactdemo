#!/usr/bin/env bash
echo Starter kit for React and Redux in ES6 on Pluralsight
echo https://github.com/coryhouse/pluralsight-redux-starter
echo [pluralsight.com] Building Applications with React and Redux in ES6 [2016, ENG]
echo рутрекер.орг/forum/viewtopic.php?t=5230809
echo "1. Скачать пакет зависимостей? [Enter]"; read dummy;
mkdir hello_world
wget https://raw.githubusercontent.com/coryhouse/pluralsight-redux-starter/master/package.json --no-check-certificate
echo "2. Установить пакеты зависимости? [Enter]"; read dummy;
npm install
echo "ИЛИ"
echo "3. Установить пакеты для Babel вручную? [Enter]"; read dummy;
npm install -g babel-cli
echo "4. Установить пакеты для Webpack вручную? [Enter]"; read dummy;
npm install webpack babel-loader webpack-dev-server --save-dev
echo "5. Установить Redux вручную? [Enter]"; read dummy;
npm install redux --save-dev