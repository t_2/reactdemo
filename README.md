#Кратко, что узнал:

 1. Установка идет через менеджер NPM 3+ пакетов от NodeJS 6+.
 2. Весь код пишется на JavaScript (Версия ECMAScript 6. Список отличий http://es6-features.org).
 3. Код ECMAScript 6 через transpiler (Вавилон) перекодируется в ECMAScript 5, который поддерживается старыми браузерами. Живой пример: https://babeljs.io/
 4. Для работы с кэшем (состояниями) используется Redux https://github.com/gaearon/redux-devtools
 5. Для уменьшения объёма передоваемых данных весь код пакуется через менеджер модулей webpack https://webpack.github.io/.
 6. Можно использовать уже настроееное окружение React https://github.com/facebookincubator/create-react-app


#Что нужно для работы:

 1. Установить плагин Chrome Facebook React (F12 вкладка React) https://chrome.google.com/webstore/detail/react-developer-tools/fmkadmapgofadopljbjfkapdkoienihi?hl=en
 2. Клонировать репозиторий https://evgeny_trushin@bitbucket.org/t_2/reactdemo.git
 3. Выполнить через copy-paste https://bitbucket.org/t_2/reactdemo/src/07d9222bb2ff2f17890a2f7ef2446ba5b591c6a5/shell_scripts/
 4. Проверить, что демо-приложение открылось на 3000 порту.

Документация на русском: https://habrahabr.ru/post/315466/
На английском: https://facebook.github.io/react/docs/hello-world.html